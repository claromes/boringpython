# This is a guess the number game
import random

print('Hello, what is your name?')
name = input()

print('Well, ' + name + ' I am thinking of a number between 1 and 20.')
secretNumber = random.randint(1, 20)

print('DEBUG:' + str(secretNumber))

for guessesTaken in range(1, 7):
    print('Take a guess.')
    
    try:
        guess = int(input())
    except ValueError:
        print('You did not enter a number.')
        continue

    if guess < secretNumber:
        print('Too low.')
    elif guess > secretNumber:
        print('Too high.')
    else:
        break # correct guess

if guess == secretNumber:
    print('Good job, ' + name + '! You guessed my number in ' + str(guessesTaken) + ' guesses!')
else:
    print('Nope! The number was ' + str(secretNumber))
