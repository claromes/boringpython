print('How many cats do you have?')
numCats = input()

try:
    if int(numCats) >= 4:
        print('A lot!')
    else:
        print('Not that many!')
except ValueError:
    print('You did not enter a number.')
