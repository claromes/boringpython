# input validation using break statement
name = ''
while True:
    print('Type your name')
    name = input()
    if name == 'your name':
        break
print('Thank you')
