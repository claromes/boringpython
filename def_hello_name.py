def hello(name): #func(parameter)
    print('Hello ' + name)

hello('Alice') #func(argument)
hello('Bob')
