print("Number:")

try:
  number = int(input())
except ValueError:
  print("Is not a number")
  
def collatz(number):  

  while number != 1:
    if (number % 2 == 0):
      number = number // 2
      print(number)
    elif (number % 2 == 1):
      number = number * 3 + 1
      print(number)

try:
  collatz(number)
except NameError:
  print("Try again")
